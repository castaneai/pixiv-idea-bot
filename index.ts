interface PixivResponse {
	error: boolean
	message: string
	body: PixivIdeaBody | null
}

interface PixivIdeaBody {
	idea_anniversary_date: string
	idea_anniversary_tag: string
	idea_anniversary_description: string
	share_data: {
		url: string
		text_and_url: string
		text_without_display_tag: string
	}
	tag_url: string
}

async function postToDiscord(webhookUrl: string, message: string) {
	const init: RequestInit = {
		method: "POST",
		headers: {
			"content-type": "application/json",
		},
		body: JSON.stringify({
			content: message,
		})
	}
	const resp = await fetch(webhookUrl, init)
	if (!resp.ok) {
		throw await resp.json()
	}
}

async function main(date: Date) {
	const webhookUrl = process.env.DISCORD_WEBHOOK_URL
	if (!webhookUrl) throw Error("env: DISCORD_WEBHOOK_URL is missing")

	// convert to YYYY-mm-dd
	const dateStr = date.toLocaleDateString('sv-SE')
	const url = `https://www.pixiv.net/ajax/idea/anniversary/${dateStr}`
	const init: RequestInit = {
		headers: {
			"referer": "https://www.pixiv.net/idea/",
		}
	}
	const resp = await fetch(url, init)
	const data: PixivResponse = await resp.json()
	const title = data.body?.share_data.text_without_display_tag
	if (!title) throw Error("title is null")
	const desc = data.body?.idea_anniversary_description
	if (!desc) throw Error("idea_anniversary_description is null")

	const tag_url = `https://pixiv.net${data.body?.tag_url}`
	const message = `${title}\n\n${desc}\n\n${tag_url}`
	console.log(message)
	await postToDiscord(webhookUrl, message)
}

main(new Date())
